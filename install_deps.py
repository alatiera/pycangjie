#!/usr/bin/python

from subprocess import run


with open('/etc/os-release') as f:
    os_info = f.read()

for line in os_info.split('\n'):
    if line.startswith('ID='):
        param, value = line.split('=')

        if value == 'fedora':
            run(['dnf', 'install', '-y', 'python-packaging'])

        elif value == 'ubuntu':
            run(['apt-get', 'install', '-y', 'python-packaging'])
